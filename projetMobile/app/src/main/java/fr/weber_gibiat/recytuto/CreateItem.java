package fr.weber_gibiat.recytuto;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


public class CreateItem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createitem_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    public void onButtonClicked(View v){
        EditText nom = ((EditText) findViewById(R.id.nomTache));
        RadioGroup rg = ((RadioGroup) findViewById(R.id.RDGroup));
        RadioButton rB = ((RadioButton) findViewById(rg.getCheckedRadioButtonId()));
        if (nom.getText().toString().length() > 0){
            TodoItem item = new TodoItem(TodoItem.Tags.valueOf(rB.getText().toString()) , nom.getText().toString());
            TodoDbHelper.addItem(item, getBaseContext());
            setResult(RESULT_OK);
            finish();
        }else{
            tToast("veuillez entrer une tache");
        }



    }

    private void  tToast(String s){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, s, duration);
        toast.show();
    }

}
