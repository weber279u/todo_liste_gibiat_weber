package fr.weber_gibiat.recytuto;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<TodoItem> items;
    private RecyclerView recycler;
    private LinearLayoutManager manager;
    private RecyclerAdapter adapter;
    private boolean onSelect;
    private static final int CODE_ACTIVITE = 1;
    public static MainActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        onSelect = false;
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                Intent i = new Intent(getCurrentFocus().getContext(), CreateItem.class);
                startActivityForResult(i, CODE_ACTIVITE);


            }
        });
        Log.i("INIT", "Fin initialisation composantes");

//         Test d'ajout d'un item
        TodoItem item = new TodoItem(TodoItem.Tags.Important, "Réviser ses cours");
        TodoDbHelper.addItem(item, getBaseContext());
        item = new TodoItem(TodoItem.Tags.Normal, "Acheter du pain");
        TodoDbHelper.addItem(item, getBaseContext());

        // On récupère les items
        items = TodoDbHelper.getItems(getBaseContext());
        Log.i("INIT", "Fin initialisation items");

        // On initialise le RecyclerView
        recycler = (RecyclerView) findViewById(R.id.recycler);
        manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);
        adapter = new RecyclerAdapter(items);
        recycler.setAdapter(adapter);
//        recycler.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), recycler, new RecyclerViewClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//                Toast.makeText(getApplicationContext(), items.get(position).getLabel() + " is long pressed!", Toast.LENGTH_SHORT).show();
////                view.setVisibility(View.INVISIBLE);
//            }
//
//        }));


        setRecyclerViewItemTouchListener();
        Log.i("INIT", "Fin initialisation recycler");

    }

    public void recyclerSet(){
        items = TodoDbHelper.getItems(getBaseContext());


        adapter = new RecyclerAdapter(items);
        recycler.setAdapter(adapter);

    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        switch (item.getOrder()){
            case 0:
                AlertDialog.Builder myPopup = new AlertDialog.Builder(this);
                myPopup.setTitle("suprimer");
                myPopup.setMessage("voulez vous vraimment suprimer cette tache");
                myPopup.setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TodoDbHelper.deleteItem((TodoItem)items.get(item.getItemId()), getBaseContext());
                        recyclerSet();
                    }
                });
                myPopup.setNegativeButton("NON", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {}
                });
                myPopup.show();

                break;
            case 1:

                break;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case CODE_ACTIVITE:
                recyclerSet();
                break;
        }
    }

    public void switchChange(int pos, boolean state){
        items.get(pos).setDone(state);
//        recyclerSet();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent dbmanager = new Intent(getCurrentFocus().getContext(), AndroidDatabaseManager.class);
            startActivity(dbmanager);
            return true;
        }

        if (id == R.id.viderListe) {
            TodoDbHelper.deleteItems(getBaseContext());
            recyclerSet();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerViewItemTouchListener() {
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                // Non géré dans cet exemple (ce sont les drags) -> on retourne false
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                TodoItem item = items.get(position);

                switch(swipeDir) {
                    case ItemTouchHelper.RIGHT:
                        item.setDone(true);
                        break;
                    case ItemTouchHelper.LEFT:
                        item.setDone(false);
                        break;
                }

                recycler.getAdapter().notifyDataSetChanged();

            }




        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recycler);
    }

    public void  tToast(String s){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, s, duration);
        toast.show();
    }

}
