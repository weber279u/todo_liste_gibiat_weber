package fr.weber_gibiat.recytuto;

import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by phil on 07/02/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.TodoHolder> {

    private ArrayList<TodoItem> items;

    public RecyclerAdapter(ArrayList<TodoItem> items) {
        this.items = items;
    }


    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new TodoHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int position) {
        TodoItem it = items.get(position);
        holder.bindTodo(it);
    }

    public void addItem(TodoItem i){
        this.items.add(i);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class TodoHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        private Resources resources;
        private ImageView image;
        private Switch sw;
        private TextView label;
        private TodoItem item;
        private View v;


        public TodoHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.imageView);
            sw = (Switch) itemView.findViewById(R.id.switch1);
            label = (TextView) itemView.findViewById(R.id.textView);
            resources = itemView.getResources();
            v =  itemView.findViewById(R.id.view);

            itemView.setOnCreateContextMenuListener(this);
            sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    MainActivity.activity.switchChange(TodoHolder.this.getAdapterPosition(), b);
                }
            });
        }



        public void bindTodo(TodoItem todo) {
            label.setText(todo.getLabel());
            sw.setChecked(todo.isDone());
            if (todo.isDone()) {
                v.setBackgroundColor(Color.GREEN);
                itemView.setBackgroundColor(Color.GREEN);
            }else {
                v.setBackgroundColor(Color.RED);
                itemView.setBackgroundColor(0);
            }
            switch(todo.getTag()) {
                case Faible:
                    image.setBackgroundColor(resources.getColor(R.color.faible));
                    break;
                case Normal:
                    image.setBackgroundColor(resources.getColor(R.color.normal));
                    break;
                case Important:
                    image.setBackgroundColor(resources.getColor(R.color.important));
                    break;

            }
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(this.getAdapterPosition(), this.getAdapterPosition(), 0, "delete");
            contextMenu.add(this.getAdapterPosition(), this.getAdapterPosition(), 1, "modifier");
        }
    }
}
